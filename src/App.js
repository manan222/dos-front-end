import React, { Component } from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import Login from "./components/Login/Login";
import Signup from "./components/Signup/Signup";
import Main from "./components/Main/Main";
import Details from "./components/Details/Details";
import GameScreen from "./components/GameScreen/GameScreen";
import Session from "./components/Session/Session";
import CheckOut from "./components/CheckOut/CheckOut";
import MerchantLists from "./components/Merchant/MerchantLists";
import store from "./redux/configure-store";

class App extends Component {
  render() {
    return (
      //We have enabled routing by wraping everything inside BrowserRouter
      <Provider store={store}>
        <BrowserRouter>
          <div className="App">
            <Switch>
              <Route path="/login" exact component={Login} />
              <Route path="/signup" exact component={Signup} />
              <Route path="/" exact component={Main} />
              <Route path="/session" exact component={Session} />
              <Route path="/details" exact component={Details} />
              <Route path="/checkout" exact component={CheckOut} />
              <Route path="/gameplay" exact component={GameScreen} />
              <Route path="/merchantlist" exact component={MerchantLists} />

              {/* <Route path="/callback" render={(props) => {
                handleAuthentication(props);
                return <Callback {...props} /> */}
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
