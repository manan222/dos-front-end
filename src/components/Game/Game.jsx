import React, { Component } from "react";
import "./game.css";
import { toast } from "react-toastify";

class Game extends Component {
  state = {
    name: "circle",
    buttonName: "Spin",
    arrowHead: "arrow",
  };

  startRotation = () => {
    this.setState({
      name: "circle start-rotate",
      buttonName: "Stop",
    });
    setTimeout(() => {
      this.setState({
        name: "circle start-rotate stop-rotate",
        buttonName: "Spin",
      });
    }, 20000);
  };
  stopRotation = () => {
    this.setState({
      name: "circle start-rotate stop-rotate",
      buttonName: "Spin",
    });
    toast.success("You  have successfuly won the offer", {
      position: toast.POSITION.TOP_CENTER,
      autoClose: 1000,
    });
  };

  render() {
    const buttonName = this.state.buttonName;
    let button;
    if (buttonName === "Spin") {
      button = (
        <button className="mb-5 spin-style" onClick={this.startRotation}>
          {this.state.buttonName}
        </button>
      );
    } else {
      button = (
        <button className="mb-5 spin-style" onClick={this.stopRotation}>
          {this.state.buttonName}
        </button>
      );
    }
    const arrowHead = this.state.arrowHead;
    if (arrowHead === "fifth-sec") {
      toast.success("Welcome to Dashboard", {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 2000,
      });
    }
    return (
      <div>
        <div className={this.state.arrowHead}></div>
        <ul className={this.state.name}>
          <li className="first-sec first-angle li-style">
            <div
              className="text"
              // contentEditable="true"
              spellCheck="false"
            >
              1
            </div>
          </li>
          <li className="second-sec second-angle li-style">
            <div
              className="text"
              // contentEditable="true"
              spellCheck="false"
            >
              2
            </div>
          </li>
          <li className="third-sec third-angle li-style">
            <div className="text" contentEditable="true" spellCheck="false">
              3
            </div>
          </li>
          <li className="fourth-sec fourth-angle li-style">
            <div
              className="text"
              // contentEditable="true"
              spellCheck="false"
            >
              4
            </div>
          </li>
          <li className="fifth-sec fifth-angle li-style">
            <div
              className="text"
              // contentEditable="true"
              spellCheck="false"
            >
              PRICE
            </div>
          </li>
          <li className="sixth-sec sixth-angle li-style">
            <div
              className="text"
              // contentEditable="true"
              spellCheck="false"
            >
              6
            </div>
          </li>
        </ul>
        {button}
      </div>
    );
  }
}

export default Game;
