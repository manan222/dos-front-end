import React, { Component } from 'react';
import Header from '../Layout/Header/Header';
import Footer from '../Layout/Footer/Footer';
import { connect } from 'react-redux';
import { fetchPosts } from '../../redux/actions/postActions';
import './Help.css';
class Main extends Component {
    componentWillMount() {

        this.props.fetchPosts();//That fetchpost() method is coming from postActions.
    }
    render() {
        const postItems = this.props.posts.map(post => //That posts object is coming from fetchPosts() method
            (
                <div key={post.id}>
                    <h5>{post.title}</h5>
                    <p>{post.body}</p>
                </div>
            ));
        return (
            <div >
                <Header />
                <h3>Posts</h3>
                <div className="post-style">
                    {postItems}   
                </div>
                <Footer />       
            </div>
        );
    }
}
const mapStateToProps = (state) => (
    {
        posts: state.posts.items
    });

export default connect(mapStateToProps, { fetchPosts })(Main);
