import React from 'react';
import  './Footer.css';
import {Link} from 'react-router-dom';
const Footer = () => {
    return (
        <div>
            <footer className="page-footer footer-style font-small  pt-4">
                <div className="container-fluid text-center text-md-left">
                    <div className="row">
                    <div className="col-md-3 mt-md-0 mt-3">
                           <h3>About us</h3>
                           <p>DOS is a plateform to provide variety of products</p>
                            <p>by different Brands.It provides you exciting offers and win exciting prizes.</p>
                        </div>
                      
                        <div className="col-md-2 mb-md-0 mb-3">
                            <h3>Links</h3>
                            <ul className="list-unstyled">
                                <li>
                                <Link to="/"  className="a-style">
                                       Home
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/merchantlist"  className="a-style">
                                        Merchant
                                    </Link>
                                    
                                </li>
                                <li>
                                <Link to="/session"  className="a-style">
                                       Session
                                    </Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3 mb-md-0 mb-3">
                            <h3>Contact Information</h3>
                            <h6>Phone</h6>
                            <p className="text-muted">+92-3242462</p>
                            <h6>Email</h6>
                            <p className="text-muted">info@luminogics.com</p>
                        </div>
                        <div className="col-md-4 mt-md-0 mt-3">
                           <h3>Address</h3>
                           <p>Office#145,GECHS,Township,Lahore Cantt</p>
                            <p>by different Brands.It provides you exciting offers and win exciting prizes.</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <p>&copy; 2020 DOS.All Copywrights are Reserved</p>
                        </div>
                    </div>
                </div>
            </footer>


        </div>
            );
        }
        
        export default Footer;
