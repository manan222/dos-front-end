import React,{Component} from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import dos_logo from '../../../assets/logo/dos_logo.png';
import './Header.css';
class Header extends Component
{
    render()
    {
        return(
            <div>
            <Navbar collapseOnSelect expand="md" className="nav-bg" >
                <Navbar.Brand to="#home">
                    <img
                        src={dos_logo}
                        className="logo-style"s
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"   />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Link to="/" className="link-style" >Home</Link>
                        <Link to="/merchantlist" className="link-style" >Merchant</Link>
                        <Link to="/session" className="link-style">Session</Link>
                    </Nav>
                    <Nav>
                        {/* <Link to="/">
                            <Button type="button" 
                            className="btn btn-info"
                                >Log Out
                            </Button>
                        </Link>        */}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
    
        </div>

        )
    }
}

export default Header;
