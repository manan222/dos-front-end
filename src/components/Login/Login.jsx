import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, Col, Button } from "react-bootstrap";
import "./Login.css";
import { toast, Zoom } from "react-toastify";
import { loginRequest } from "../../services/dataService";
import { connect } from "react-redux";
import { loginSuccess } from "../../redux/actions";
import LoadingSpinner from "../UI/LoadingSpinner/LoadingSpinner";
import { NETWORK_ERROR } from "../../constants/Constants";

toast.configure();

class Login extends Component {
  state = {
    user: {
      email: "",
      password: "",
    },
    isLoading: false,
  };
  submitHandler = async (event) => {
    event.preventDefault();
    const { user } = this.state;
    console.log("Email from Login Component=", user.email);
    console.log("Password Login Component=", user.password);
    if (user.email !== "" && user.password !== "") {
      this.setState({ isLoading: true });
      let loginObj = {
        email: user.email,
        password: user.password,
      };
      let loginResponse = await loginRequest(loginObj);
      if (loginResponse === NETWORK_ERROR) {
        toast.error(NETWORK_ERROR);
        this.setState({ isLoading: false });
      } else {
        // console.log("response=",loginResponse)
        console.log("Response=", loginResponse.data.metadata.message);
        const status = loginResponse.data.metadata.status;
        const msg = loginResponse.data.metadata.message;
        if (status === "SUCCESS") {
          this.props.loginSuccess(loginResponse); 
          this.setState({ isLoading: false });

          toast.success(msg, {
            position: toast.POSITION.TOP_CENTER,
            transition: Zoom,
            autoClose: 3000,
          });
          this.props.history.push("/gameplay");
        } else {
          this.setState({ isLoading: false });
          toast.warning(msg, {
            position: toast.POSITION.TOP_CENTER,
            transition: Zoom,
            autoClose: 3000,
          });
        }
      }
    }
  };
  onChange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value,
      },
    });
  };

  render() {
    toast.configure({
      position: toast.POSITION.TOP_CENTER,
      transition: Zoom,
      autoClose: 3000,
    });
    const { isLoading } = this.state;
    return (
      <div className="container-fluid bg-img">
        <div className="row  ">
          <div className="col-lg-4"></div>
          <div className="col-lg-4 ">
            <Form onSubmit={this.submitHandler}>
              <Form.Row>
                <Form.Group as={Col} md="10" controlId="validationCustom01">
                  <h4 className="title-style">First Login To Play Game </h4>
                  <Form.Label className="label-style">Email:</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    autoComplete="off"
                    name="email"
                    placeholder=" Enter Email"
                    onChange={this.onChange}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="10" controlId="validationCustom02">
                  <Form.Label className="label-style">Password:</Form.Label>
                  <Form.Control
                    required
                    type="password"
                    name="password"
                    autoComplete="off"
                    placeholder=" Enter Password"
                    onChange={this.onChange}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                {isLoading ? (
                  <LoadingSpinner />
                ) : (
                  <Button type="submit" className="btn2-style">
                    Login
                  </Button>
                )}

                <Link to="/signup">
                  <Button className=" btn  btn2-style ml-4 ">
                    Create Account
                  </Button>
                </Link>
                <Link to="/details">
                  <Button className=" btn btn2-style ml-4">Cancel</Button>
                </Link>
              </Form.Row>
            </Form>
          </div>
          <div className="col-lg-4"></div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    offers: state.availableOffers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // fetchOffers: offerData => dispatch(fetchOffers(offerData)),
    loginSuccess: (userData) => dispatch(loginSuccess(userData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
