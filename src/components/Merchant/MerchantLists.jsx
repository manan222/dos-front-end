import React, { Component } from "react";
import "./MerchantLists.css";
import Header from "../Layout/Header/Header";
import Footer from "../Layout/Footer/Footer";
import {fetchMerchantsRequest} from "../../services/dataService";
import { fetchMerchants } from "../../redux/actions";
import { Card, CardBody, CardHeader, CardTitle } from "reactstrap";
import DataTable from "react-data-table-component";
import { connect } from "react-redux";
import { NETWORK_ERROR } from "../../constants/Constants";
import { toast,Zoom } from "react-toastify";

const columns  = [
  {
    name: "Name",
    selector: "name",
  },
  {
    name: "Phone",
    selector: "phone",
  },

  {
    name: "Type",
    selector: "type",
    sortable: true,
  },
  {
    name: "Date Created",
    selector: "thumb",
    sortable: true,
  },
  {
    name: "Time Created",
    selector: "added_date",
    sortable: true,
  },
  {
    name: "Category",
    selector: "category",
    sortable: true,
  },
];
class MerchantLists extends Component {
  async componentDidMount() {
  var merchantResponse=await fetchMerchantsRequest();
  if(merchantResponse===NETWORK_ERROR)
  {
  }
  else
  {
    this.props.fetchMerchants(merchantResponse);
  }

  }
  render() {
    const { merchants } = this.props;
    toast.configure(
      {
        position: toast.POSITION.TOP_CENTER,
        transition:Zoom,
        autoClose:3000
      }
    )
  
    merchants.map((ms) => {
      const date = ms.added_date;
      const updatedDate=date.split('T')[0];
      ms.thumb=updatedDate;
      var time = new Date(date);
      var useroptions = {
        hour: "numeric",
        minute: "numeric",
        hour12: true,
      };
      return ms.added_date= time.toLocaleString("en-US", useroptions);
      // return ms.added_date;
    });
  merchants.map((ms)=>
  {      const phone =ms.phone;
    const stringPhone=phone + "";
    const final=stringPhone.replace(stringPhone,"0"+stringPhone);
    ms.phone=final;
    return ms.phone;

  })
    return (
      <div>
        <Header />
        <Card className="table-style">
          <CardHeader>
            <CardTitle>Merchant Details</CardTitle>
          </CardHeader>
          <CardBody>
            <DataTable data={merchants} columns={columns} noHeader pagination />
          </CardBody>
        </Card>
        <Footer />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    merchants: state.merchants,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchMerchants: (merchantData) => dispatch(fetchMerchants(merchantData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MerchantLists);
