import React from 'react';
import { Card } from 'react-bootstrap';
import item1 from '../../assets/mall_items/item1.jpg';
import item2 from '../../assets/mall_items/item2.jpg';
import item4 from '../../assets/mall_items/item4.jpg';
import item5 from '../../assets/mall_items/item5.jpg';
import item6 from '../../assets/mall_items/item6.jpg';
import face_wash from '../../assets/mall_items/face_wash.jpg';
import grocery from '../../assets/mall_items/grocery.jpg';
import color_brush from '../../assets/mall_items/color_brush.jpg';
import mobile_cover from '../../assets/mall_items/mobile_cover.jpg';
import './OnlineMall.css';
const OnlineMall = () => {
    return (
        <div>
            <h4 className="h4-style">Online Mall</h4>
            <div className="flex-container">
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={item1}
                                className="img3-style"
                                alt="pic1"
                            />
                        </div>
                        <Card.Text className="text-center padding" >
                                <span className="p1-style" >Sega Traders</span><br></br>
                                <span className="text-muted p2-style">Sega Traders</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={item2}
                                className="img3-style" alt="infant" />
                        </div>
                        <Card.Text className="text-center padding" >
                            <span className="p1-style">Vital </span><br></br>
                            <span className="text-muted p2-style">Strong and Perfect</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={mobile_cover}
                                className="img3-style" alt="infant" />
                        </div>
                        <Card.Text className="text-center padding" >

                            <span className="p1-style ">Speed Private Limited</span><br></br>
                            <span className="text-muted p2-style">Speed Private Limited</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={item4}
                                className="img3-style" alt="infant" />
                        </div>
                        <Card.Text className="text-center padding" >
                            <span className="p1-style">WB By Hemani</span> <br></br>
                            <span className="text-muted p2-style">WB By Hemani</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={item5}
                                className="img3-style" alt="infant" />
                        </div>
                        <Card.Text className="text-center padding" >
                            <span className="p1-style">Hommold</span><br></br>
                            <span className="text-muted p2-style">Hommold</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={item6}
                                className="img3-style" alt="infant" />
                        </div>
                        <Card.Text className="text-center padding">
                            <span className="p1-style">Fun WorldPk</span><br></br>
                            <span className="text-muted p2-style">Toys and Baby Store</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={face_wash}
                                className="img3-style"
                                alt="pic1"
                            />
                        </div>
                        <Card.Text className="text-center padding" >
                                <span className="p1-style" >Sega Traders</span><br></br>
                                <span className="text-muted p2-style">Sega Traders</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={color_brush}
                                className="img3-style"
                                alt="pic1"
                            />
                        </div>
                        <Card.Text className="text-center padding" >
                                <span className="p1-style" >Sega Traders</span><br></br>
                                <span className="text-muted p2-style">Sega Traders</span>
                        </Card.Text>
                    </Card>
                </div>
                <div>
                    <Card className="card3-style">
                        <div className="text-center">
                            <img src={grocery}
                                className="img3-style"
                                alt="pic1"
                            />
                        </div>
                        <Card.Text className="text-center padding" >
                                <span className="p1-style" >Sega Traders</span><br></br>
                                <span className="text-muted p2-style">Sega Traders</span>
                        </Card.Text>
                    </Card>
                </div>
            </div>
        </div>
    );
}

export default OnlineMall;