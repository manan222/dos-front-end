import React from 'react';
import Header from '../Layout/Header/Header';
import Footer from '../Layout/Footer/Footer';
import { toast } from 'react-toastify';
import { Card } from 'react-bootstrap';
import './Plans.css';


toast.configure()
const Aboutus = () => {
    return (
        <div>
            <Header />
            <div className="about-section">
                <h1>Plans</h1>
                <p>A boiler plate project to have basic build up structure for all React Projects</p>
                <p>It consists of Authentication,Routing,Components,Unit Tests</p>
            </div>
            <div className="row margin2">
                <div className="col-lg-3">
                    <Card className="card22-style">
                        <Card.Body>
                            <Card.Title className="plan-title">Plan 1</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Standard</Card.Subtitle>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's content.
                                </Card.Text>
                            <Card.Link href="#">Card Link</Card.Link>
                            <Card.Link href="#">Another Link</Card.Link>
                        </Card.Body>
                    </Card>
                </div>
                <div className="col-lg-3">
                    <Card className="card22-style">
                        <Card.Body>
                            <Card.Title className="plan-title">Plan 2</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Premium</Card.Subtitle>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's content.
                                </Card.Text>
                            <Card.Link href="#">Card Link</Card.Link>
                            <Card.Link href="#">Another Link</Card.Link>
                        </Card.Body>
                    </Card>
                </div>
                <div className="col-lg-3">
                    <Card className="card22-style">
                        <Card.Body>
                            <Card.Title className="plan-title">Plan 3</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Pro</Card.Subtitle>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's content.
                                </Card.Text>
                            <Card.Link href="#">Card Link</Card.Link>
                            <Card.Link href="#">Another Link</Card.Link>
                        </Card.Body>
                    </Card>
                </div>
                <div className="col-lg-3">
                    <Card className="card22-style">
                        <Card.Body>
                            <Card.Title className="plan-title">Plan 4</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Free </Card.Subtitle>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's content.
                                </Card.Text>
                            <Card.Link href="#">Card Link</Card.Link>
                            <Card.Link href="#">Another Link</Card.Link>
                        </Card.Body>
                    </Card>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Aboutus;