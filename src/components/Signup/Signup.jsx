import React, { Component } from "react";
import { Form, Col, Button } from "react-bootstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./Signup.css";
import { Link } from "react-router-dom";
import { registerRequest } from "../../services/dataService";
import { registerSuccess } from "../../redux/actions";
import { connect } from "react-redux";
import { NETWORK_ERROR } from "../../constants/Constants";
import LoadingSpinner from "../UI/LoadingSpinner/LoadingSpinner";
class Signup extends Component {
  state = {
    user: {
      email: "",
      password: "",
    },
    isLoading: false,
  };
  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value,
      },
    });
  };
  submitHandler = async (event) => {
    event.preventDefault();
    const { user } = this.state;
    console.log("email=" + user.email);
    console.log("Password=" + user.password);
    if (user.email !== "" && user.password !== "") {
      this.setState({ isLoading: true });
      let registerResponse = await registerRequest(user);
      if (registerResponse === NETWORK_ERROR) {
        toast.error(NETWORK_ERROR);
        this.setState({ isLoading: true });
      } else {
        console.log("Response=", registerResponse.data.metadata.message);
        const msg = registerResponse.data.metadata.message;
        const status = registerResponse.data.metadata.status;
        if (status === "SUCCESS") {
          this.props.registerSuccess(registerResponse);
          this.setState({ isLoading: false });
          this.props.history.push("/login");
          toast.success(msg, {
            position: toast.POSITION.TOP_CENTER,
            autoClose: 3000,
          });
        } else {
          this.setState({ isLoading: false });
          console.log("msg=", msg);
          toast.warning(msg, {
            position: toast.POSITION.TOP_CENTER,
            autoClose: 3000,
          });
        }
      }
    }
  };

  render() {
    const { isLoading } = this.state;

    return (
      <div className="container-fluid bg-img ">
        <div className="row">
          <div className="col-lg-4 offset-4">
            <Form onSubmit={this.submitHandler}>
              <Form.Row>
                <Form.Group as={Col} md="10" controlId="validationCustom01">
                  <h4 className="title-style">Create Account</h4>
                  <Form.Label className="label-style">Email:</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    name="email"
                    autoComplete="off"
                    placeholder=" Enter Email"
                    onChange={this.handleChange}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="10" controlId="validationCustom02">
                  <Form.Label className="label-style">Password:</Form.Label>
                  <Form.Control
                    required
                    type="password"
                    autoComplete="off"
                    name="password"
                    placeholder=" Enter Password"
                    onChange={this.handleChange}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                {isLoading ? (
                  <LoadingSpinner />
                ) : (
                  <Button type="submit" className="btn2-style">
                    Confirm
                  </Button>
                )}

                <Link to="/login">
                  <Button type="submit" className=" btn2-style  ml-4">
                    Login
                  </Button>
                </Link>
              </Form.Row>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerSuccess: (userData) => dispatch(registerSuccess(userData)),
  };
};

export default connect(null, mapDispatchToProps)(Signup);
