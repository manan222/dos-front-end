import React,{useState} from 'react';
import {Carousel} from 'react-bootstrap';
import bg4 from '../../../assets/background_images/bg4.jpg';
import bg7 from '../../../assets/background_images/bg7.jpg';
import bg8 from '../../../assets/background_images/bg8.jpg';
import bg9 from '../../../assets/background_images/bg9.jpg';
import bg10 from '../../../assets/background_images/bg10.jpg';
import bg11 from '../../../assets/background_images/bg11.webp';
import bg12 from '../../../assets/background_images/bg12.webp';
import bg13 from '../../../assets/background_images/bg13.webp';
import './Carousel.css';
export default function ControlledCarousel() {
    const [index, setIndex] = useState(0);
    const [direction, setDirection] = useState(null);
  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
      setDirection(e.direction);
    };
  
    return (
      <div className="container-fluid">
      <Carousel activeIndex={index} direction={direction} onSelect={handleSelect}>
        <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg7}     
            alt="First slide"
          />
        </Carousel.Item>    
       <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg8}
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg9}
            alt="Third slide"
          />
        </Carousel.Item>
        <Carousel.Item className="text-center ">
          <img
            className="slider-img"
            src={bg10}    
            alt="Fourth slide"
          />
        </Carousel.Item>
        <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg11} 
            alt="Fifth slide"
          />
        </Carousel.Item>
        <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg12}
            alt="Sixth slide"
          />
        </Carousel.Item>
        <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg4}
            alt="Seventh slide"
          />
        </Carousel.Item>
        <Carousel.Item className="text-center">
          <img
            className="slider-img"
            src={bg13}
            alt="Eight slide"
          />
        </Carousel.Item>
      </Carousel>
      </div>
    );
  }