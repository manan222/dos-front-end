import React from "react";
import { Button, Spinner } from "react-bootstrap";

const LoadingSpinner = () => {
  return (
    <div>
      <Button className="btn2-style">
        <Spinner color="white" size="sm" />
        <span className="ml-50">Loading...</span>
      </Button>
    </div>
  );
};

export default LoadingSpinner;
