import React, { Component } from 'react';
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';


class Modal extends Component {
state={
  modal5: false
}

toggle = nr => () => {
  let modalNumber = 'modal' + nr
  this.setState({
    [modalNumber]: !this.state[modalNumber]
  });
}


render() {
  return (
    <MDBContainer>
      <MDBModal isOpen={this.state.modal5} toggle={this.toggle(5)} size="fluid">
        <MDBModalHeader toggle={this.toggle(5)}>Game Play Instructions</MDBModalHeader>
        <MDBModalBody>
                <ol>
                    <li><p className="instructions"><b>Click The Spin button to spin wheel.</b></p></li>
                    <li><p className="instructions"><b>There will be different sections inside a circle.</b></p></li>
                    <li><p className="instructions"> <b>Stop your wheel Price section right beneath the arrow head.</b> </p></li>
                    <li><p className="instructions"><b>Click Stop Button during rotation to stop it.</b></p></li>
                </ol>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="secondary" onClick={this.toggle(5)}>Close</MDBBtn>
          <MDBBtn color="primary">Save changes</MDBBtn>
        </MDBModalFooter>
      </MDBModal>
    </MDBContainer>
    );
  }
}

export default Modal;