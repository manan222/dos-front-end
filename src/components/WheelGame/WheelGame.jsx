import StorageService from '../../services/storageService';
// import Phaser from 'react-phaser';
import React, { Component } from 'react';
import { DataService } from '../../services/dataService';
import {WheelPhaser} from '../Phaser/Phaser';
import Phaser from 'phaser';
export class WheelGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      storageService: StorageService,
      dataService: DataService
    }
    console.log(this.state.storageService)
    console.log("constructor");
  }
  componentDidMount() {
    let wheelPhaserObj = new WheelPhaser(this.dataService);

    const gameConfig = {
      type: Phaser.CANVAS,
      width: 1200,
      height: 600,
      backgroundColor: 0xffffff,
      parent: Phaser,
      scene: wheelPhaserObj
    };
    this.game = new Phaser.Game(gameConfig);
    console.log(this.game.scene.scenes)
    this.resize();
    const mode = this.route.snapshot.paramMap.get('mode')
    console.log("mode =", mode)
    this.storageService.setItem("mode", mode);

    const offerId = this.route.snapshot.paramMap.get("offerId")
    this.storageService.setItem("offerId", offerId);
  }
  componentWillUnmount() {
    const canvas = document.getElementsByTagName("canvas")[0];
    const body = document.getElementsByTagName("body");
    // console.log(body);
    body[0].removeChild(canvas);
    this.game.destroy();
  }


  veryEasyDD() {
    this.game.scene.scenes[0].setVeryEasyMode();
  }

  easyDD() {
    this.game.scene.scenes[0].setEasyMode();
    // console.log("easy: Wheel Componenet");
  }

  mediumDD() {
    this.game.scene.scenes[0].setMediumMode();
    // console.log("medium: Wheel Component")
  }

  hardDD() {
    this.game.scene.scenes[0].setHardMode();
    // console.log("hard: Wheel Component")
  }

  veryHardDD() {
    this.game.scene.scenes[0].setVeryHardMode();
    // console.log("hard: Wheel Component")
    // console.log("very hard component")
  }

  resize() {
    const canvas = document.querySelector("canvas");
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;
    const windowRatio = windowWidth / windowHeight;
    const gameRatio = this.game.config.width / this.game.config.height;
    if (windowRatio < gameRatio) {
      canvas.style.width = windowWidth + "px";
      canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else {
      canvas.style.width = (windowHeight * gameRatio) + "px";
      canvas.style.height = windowHeight + "px";
    }
  }
  reder() {
    return (
      <div id="phaser-game">

      </div>

    );
  }
}



