export const BASE_URL = "https://dos-node.herokuapp.com/"
export const OFFER_URL = "offer/api/v1/fetchOffers";
export const REGISTER_URL = "admin/api/v1/signUp";
export const LOGIN_URL = "admin/api/v1/login";
export const POST_URL = "http://jsonplaceholder.typicode.com/users";
export const MERCHANT_URL = "merchant/api/v1/fetchMerchants";
export const INFANT = "../assets/categories/infant.jpg";
export const obj = {
    "store": "store_2dddbe41b7004652a2fd7addfb836847",
    "pageSize": "500",
    "offset": "0"
}
export const UNITY = "https://mdbootstrap.com/img/logo/brands/unity.png";
export const YAHOO = "https://mdbootstrap.com/img/logo/brands/yahoo.png";
export const AMAZON = "https://mdbootstrap.com/img/logo/brands/amazon.png";
export const NIKEE = "https://mdbootstrap.com/img/logo/brands/nike.png";
export const SAMSUNG = "https://mdbootstrap.com/img/logo/brands/samsung.png";
export const EMAIL = "manan@luminogics.com";
export const PASSWORD = "alibutt123";
export const CREATE_SESSION_URL = "dos/api/v1/session/create";
export const CLOSE_SESSION_URL = "dos/api/v1/session/close";
export const NETWORK_ERROR = "Sorry,Your Request Cannot be completed.Please Check Your Internet Connection";