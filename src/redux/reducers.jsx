import {
  FETCH_OFFERS,
  FETCH_MERCHANTS,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  FETCH_DETAILS,
} from "./action-types";

const INITIALSTATE = {
  availableOffers: [],
  merchants: [],
  offerDetails: [],
  user: {},
};

export default (state = INITIALSTATE, action) => {
  switch (action.type) {
    case REGISTER_SUCCESS: {
      const user = action.payload;
      console.log("Updated State of user from REGISTER reducer=", user);
      return {
        ...state,
        user: user,
      };
    }
    case LOGIN_SUCCESS: {
      const user = action.payload;
      console.log("Updated State of user from LOGIN reducer=", user);
      return {
        ...state,
        user: user,
      };
    }
    case FETCH_OFFERS: {
      const newAvailableOffers = action.payload;
      return {
        ...state,
        availableOffers: newAvailableOffers,
      };
    }
    case FETCH_DETAILS: {
      const offerDetails = action.payload;
      return {
        ...state,
        offerDetails: offerDetails,
      };
    }
    case FETCH_MERCHANTS: {
      const newMerchants = action.payload;
      console.log("Updated State of merchants from reducer=", newMerchants);
      return {
        ...state,
        merchants: newMerchants,
      };
    }
    default:
      return state;
  }
};
