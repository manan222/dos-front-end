import axios from "axios";
import {
    BASE_URL,
    OFFER_URL,
    LOGIN_URL,
    MERCHANT_URL,
    REGISTER_URL,
    NETWORK_ERROR,
} from "../constants/Constants";

export async function loginRequest(obj) {
    try {
        const url = BASE_URL + LOGIN_URL;
        console.log("Email from Dataservice=", obj.email);
        console.log("Password from Dataservice=", obj.password);
        return await axios.post(url, obj);
    } catch (e) {
        return NETWORK_ERROR;

    }
}
export async function registerRequest(obj) {
    try {
        const url = BASE_URL + REGISTER_URL;
        console.log("Email from Dataservice=", obj.email);
        console.log("Password from Dataservice=", obj.password);
        return await axios.post(url, obj);
    } catch (e) {
        return NETWORK_ERROR;
    }
}

export async function fetchOffersRequest() {
    try {
        return await axios.post(BASE_URL + OFFER_URL);
    } catch (e) {
        return NETWORK_ERROR;
    }
}
export async function fetchMerchantsRequest() {
    console.log("response=");
    try {
        return await axios.post(BASE_URL + MERCHANT_URL);
    } catch (e) {
        return NETWORK_ERROR;
    }
}

// async createSession(sessionBodyobj){
//   let apiResponse = await axios.post(CREATE_SESSION_URL, sessionBodyobj).toPromise();
//   return (apiResponse.json())
// }

// async closeSession(closeSessionBodyObj){
//   let apiResponse = await axios.post(CLOSE_SESSION_URL ,closeSessionBodyObj).toPromise();
//   return (apiResponse.json())
// }